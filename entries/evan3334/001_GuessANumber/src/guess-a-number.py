#!/usr/bin/python3
import random
def num(message, allow_negative=False, allow_zero=False):
    while True:
        n=input(message)
        try:
            n=int(n)
            if not allow_negative and n<0:
                print("You can't use a negative number! Please try again.")
            elif not allow_zero and n==0:
                print("You can't use zero! Please try again.")
            else:
                return n
        except ValueError:
            print("That isn't a valid number! Please try again.")

guessed=False
tries=0
print("""Welcome to Guess a Number!
Here's how it works: I'll pick a random number and you try to guess it.""")
maxnum = num("Please enter the highest number I can pick: ")
print("Great. I'll pick a number between 0 and "+str(maxnum)+".")
correct = random.randint(0,maxnum)
while not guessed:
    guess = num("Try to guess the number! ",allow_zero=True)
    tries+=1
    if guess!=correct:
        print("Nope, try again! Tries so far: "+str(tries))
    else:
        guessed=True
        print("You got it! It took you "+str(tries)+" tries.")
    